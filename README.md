# Kubectl
[![pipeline status](https://gitlab.com/gghk2022/technology/system/utils/kubectl/badges/master/pipeline.svg)](https://gitlab.com/gghk2022/technology/system/utils/kubectl/commits/master)

`kubectl-auth`
---

The required environment variables for using `kubectl-auth` for deployment:

```
K8S_CA  # K8S CA Bundle
K8S_API # K8S Master IP:Port
K8S_TKN # K8S Service Account Token
K8S_NS  # K8S Namespace
```

`gitlab-kubectl-auth`
---
Updates to kubectl to be compatible with Gitlab-Managed Kubernetes Clusters. See the following links:
- https://docs.gitlab.com/ee/user/project/clusters/index.html#deploying-to-a-kubernetes-cluster
- https://gitlab.com/gitlab-examples/kubernetes-deploy