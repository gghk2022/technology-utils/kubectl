#!/usr/bin/env bash

ensure_environment_url() {
  echo "Reading CI_ENVIRONMENT_URL from .gitlab-ci.yml..."
  CI_ENVIRONMENT_URL="$(ruby -ryaml -e 'puts YAML.load_file(".gitlab-ci.yml")[ENV["CI_BUILD_NAME"]]["environment"]["url"]')"
  CI_ENVIRONMENT_URL="$(eval echo "$CI_ENVIRONMENT_URL")"
  echo "CI_ENVIRONMENT_URL: $CI_ENVIRONMENT_URL"
}

ensure_deploy_variables() {
  _check_env "CI_ENVIRONMENT_URL" $CI_ENVIRONMENT_URL
  _check_env "KUBE_NAMESPACE" $KUBE_NAMESPACE
  _check_env "CI_ENVIRONMENT_SLUG" $CI_ENVIRONMENT_SLUG
}
_check_env() {
  if [ -z ${2+x} ]; then
    echo "ERROR: Environment variable '$1' is not set."
    exit 1
  fi
}

create_kubeconfig() {
  [[ -z "$KUBE_URL" ]] && return

  echo "Generating kubeconfig..."
  export KUBECONFIG="$(pwd)/kubeconfig"
  export KUBE_CLUSTER_OPTIONS=
  if [[ -n "$KUBE_CA_PEM" ]]; then
    echo "Using KUBE_CA_PEM..."
    echo "$KUBE_CA_PEM" > "$(pwd)/kube.ca.pem"
    export KUBE_CLUSTER_OPTIONS=--certificate-authority="$(pwd)/kube.ca.pem"
  fi
  kubectl config set-cluster gitlab-deploy --server="$KUBE_URL" \
    $KUBE_CLUSTER_OPTIONS
  kubectl config set-credentials gitlab-deploy --token="$KUBE_TOKEN" \
    $KUBE_CLUSTER_OPTIONS
  kubectl config set-context gitlab-deploy \
    --cluster=gitlab-deploy --user=gitlab-deploy \
    --namespace="$KUBE_NAMESPACE"
  kubectl config use-context gitlab-deploy
  echo ""
}

test_kubeconfig() {
  # Test if authentication is successful
  RESPONSE=$(kubectl auth can-i create deployment)
  if [ "$RESPONSE" == "no" ]; then
    echo "The service account doesn't have permission to deploy."
    exit 1
  else
    echo "Ready for deployment, client and server info:"
    kubectl version --short
  fi
}
